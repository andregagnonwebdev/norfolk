<?php

function people_init() {
	register_post_type( 'people', array(
		'labels'            => array(
			'name'                => __( 'People', 'norfolk' ),
			'singular_name'       => __( 'Person', 'norfolk' ),
			'all_items'           => __( 'All People', 'norfolk' ),
			'new_item'            => __( 'New Person', 'norfolk' ),
			'add_new'             => __( 'Add New', 'norfolk' ),
			'add_new_item'        => __( 'Add New Person', 'norfolk' ),
			'edit_item'           => __( 'Edit Person', 'norfolk' ),
			'view_item'           => __( 'View Person', 'norfolk' ),
			'search_items'        => __( 'Search People', 'norfolk' ),
			'not_found'           => __( 'No People found', 'norfolk' ),
			'not_found_in_trash'  => __( 'No People found in trash', 'norfolk' ),
			'parent_item_colon'   => __( 'Parent Person', 'norfolk' ),
			'menu_name'           => __( 'People', 'norfolk' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-businessman',
		'show_in_rest'      => true,
		'rest_base'         => 'people',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'people_init' );

function people_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['people'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Person updated. <a target="_blank" href="%s">View Person</a>', 'norfolk'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'norfolk'),
		3 => __('Custom field deleted.', 'norfolk'),
		4 => __('Person updated.', 'norfolk'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Person restored to revision from %s', 'norfolk'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Person published. <a href="%s">View Person</a>', 'norfolk'), esc_url( $permalink ) ),
		7 => __('Person saved.', 'norfolk'),
		8 => sprintf( __('Person submitted. <a target="_blank" href="%s">Preview Person</a>', 'norfolk'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Person scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Person</a>', 'norfolk'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Person draft updated. <a target="_blank" href="%s">Preview Person</a>', 'norfolk'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'people_updated_messages' );
