<?php

function collection_init() {
	register_post_type( 'collection', array(
		'labels'            => array(
			'name'                => __( 'Collections', 'norfolk' ),
			'singular_name'       => __( 'Collection', 'norfolk' ),
			'all_items'           => __( 'All Collections', 'norfolk' ),
			'new_item'            => __( 'New Collection', 'norfolk' ),
			'add_new'             => __( 'Add New', 'norfolk' ),
			'add_new_item'        => __( 'Add New Collection', 'norfolk' ),
			'edit_item'           => __( 'Edit Collection', 'norfolk' ),
			'view_item'           => __( 'View Collection', 'norfolk' ),
			'search_items'        => __( 'Search Collections', 'norfolk' ),
			'not_found'           => __( 'No Collections found', 'norfolk' ),
			'not_found_in_trash'  => __( 'No Collections found in trash', 'norfolk' ),
			'parent_item_colon'   => __( 'Parent Collection', 'norfolk' ),
			'menu_name'           => __( 'Collections', 'norfolk' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'collection',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'collection_init' );

function collection_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['collection'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Collection updated. <a target="_blank" href="%s">View Collection</a>', 'norfolk'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'norfolk'),
		3 => __('Custom field deleted.', 'norfolk'),
		4 => __('Collection updated.', 'norfolk'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Collection restored to revision from %s', 'norfolk'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Collection published. <a href="%s">View Collection</a>', 'norfolk'), esc_url( $permalink ) ),
		7 => __('Collection saved.', 'norfolk'),
		8 => sprintf( __('Collection submitted. <a target="_blank" href="%s">Preview Collection</a>', 'norfolk'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Collection scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Collection</a>', 'norfolk'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Collection draft updated. <a target="_blank" href="%s">Preview Collection</a>', 'norfolk'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'collection_updated_messages' );
