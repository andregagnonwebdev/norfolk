<?php

function event_init() {
	register_post_type( 'event', array(
		'labels'            => array(
			'name'                => __( 'Events', 'norfolk' ),
			'singular_name'       => __( 'Event', 'norfolk' ),
			'all_items'           => __( 'All Events', 'norfolk' ),
			'new_item'            => __( 'New Event', 'norfolk' ),
			'add_new'             => __( 'Add New', 'norfolk' ),
			'add_new_item'        => __( 'Add New Event', 'norfolk' ),
			'edit_item'           => __( 'Edit Event', 'norfolk' ),
			'view_item'           => __( 'View Event', 'norfolk' ),
			'search_items'        => __( 'Search Events', 'norfolk' ),
			'not_found'           => __( 'No Events found', 'norfolk' ),
			'not_found_in_trash'  => __( 'No Events found in trash', 'norfolk' ),
			'parent_item_colon'   => __( 'Parent Event', 'norfolk' ),
			'menu_name'           => __( 'Events', 'norfolk' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'has_archive'       => true,
		'rewrite'           => true,
		//'rewrite'           => array('slug' => 'goings-on/event'),
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'event',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'event_init' );

function event_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['event'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Event updated. <a target="_blank" href="%s">View Event</a>', 'norfolk'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'norfolk'),
		3 => __('Custom field deleted.', 'norfolk'),
		4 => __('Event updated.', 'norfolk'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Event restored to revision from %s', 'norfolk'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Event published. <a href="%s">View Event</a>', 'norfolk'), esc_url( $permalink ) ),
		7 => __('Event saved.', 'norfolk'),
		8 => sprintf( __('Event submitted. <a target="_blank" href="%s">Preview Event</a>', 'norfolk'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Event scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Event</a>', 'norfolk'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Event draft updated. <a target="_blank" href="%s">Preview Event</a>', 'norfolk'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'event_updated_messages' );
