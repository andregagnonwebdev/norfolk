<?php

function exhibition_init() {
	register_post_type( 'exhibition', array(
		'labels'            => array(
			'name'                => __( 'Exhibitions', 'norfolk' ),
			'singular_name'       => __( 'Exhibition', 'norfolk' ),
			'all_items'           => __( 'All Exhibitions', 'norfolk' ),
			'new_item'            => __( 'New Exhibition', 'norfolk' ),
			'add_new'             => __( 'Add New', 'norfolk' ),
			'add_new_item'        => __( 'Add New Exhibition', 'norfolk' ),
			'edit_item'           => __( 'Edit Exhibition', 'norfolk' ),
			'view_item'           => __( 'View Exhibition', 'norfolk' ),
			'search_items'        => __( 'Search Exhibitions', 'norfolk' ),
			'not_found'           => __( 'No Exhibitions found', 'norfolk' ),
			'not_found_in_trash'  => __( 'No Exhibitions found in trash', 'norfolk' ),
			'parent_item_colon'   => __( 'Parent Exhibition', 'norfolk' ),
			'menu_name'           => __( 'Exhibitions', 'norfolk' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'exhibition',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'exhibition_init' );

function exhibition_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['exhibition'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Exhibition updated. <a target="_blank" href="%s">View Exhibition</a>', 'norfolk'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'norfolk'),
		3 => __('Custom field deleted.', 'norfolk'),
		4 => __('Exhibition updated.', 'norfolk'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Exhibition restored to revision from %s', 'norfolk'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Exhibition published. <a href="%s">View Exhibition</a>', 'norfolk'), esc_url( $permalink ) ),
		7 => __('Exhibition saved.', 'norfolk'),
		8 => sprintf( __('Exhibition submitted. <a target="_blank" href="%s">Preview Exhibition</a>', 'norfolk'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Exhibition scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Exhibition</a>', 'norfolk'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Exhibition draft updated. <a target="_blank" href="%s">Preview Exhibition</a>', 'norfolk'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'exhibition_updated_messages' );
