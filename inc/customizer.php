<?php
/**
 * NORFOLK Theme Customizer
 *
 * @package NORFOLK
 */


function norfolk_theme_mod_default( $key)
{
    static $norfolk_theme_mod_defaults = array(

      'norfolk-color-nav-link' => '#93282a',
      'norfolk-color-text-heading' => '#365684',
      'norfolk-color-text-body' => '#000000',
      'norfolk-color-text-link' => '#93282a',
      'norfolk-color-text-footer' => '#000000',
      'norfolk-color-text-footer-link' => '#93282a',
      'norfolk-color-text-footer-rollover' => '#93282a',
      'norfolk-color-rule-lines' => '#cecece',
      'norfolk-color-call-to-action' => '#365684',

      'norfolk-color-background-body' => '#fdfdfd',
      'norfolk-color-background-content' => '#efe9e5',

      'norfolk-font-header' =>  'Georgia',
      'norfolk-font-nav-menu' =>  'Arial',
      'norfolk-font-heading' =>  'Georgia',
      'norfolk-font-body-text' =>  'Arial',
      'norfolk-font-button' =>  'Arial',
      'norfolk-font-caption' =>  'Georgia',
      'norfolk-font-footer' =>  'R',


      'norfolk-facebook' => '',
      'norfolk-instagram' => '',
      'norfolk-yelp' => '',

      'norfolk-company-name' => '',
      'norfolk-address' => '',
      'norfolk-address-2' => '',
      'norfolk-town-state-zip' => '',
      'norfolk-telephone' => '860-542-5761',
      'norfolk-email' => '',

      'norfolk-copyright-message' => '© Copyright 2017. All rights reserved.',

      'norfolk-img-upload' => '',   // footer
    );

    if ( array_key_exists($key, $norfolk_theme_mod_defaults) )
        return( $norfolk_theme_mod_defaults[ $key]);
    else
    {
        return( '');
    }
}

function norfolk_get_theme_mod( $key, $default='unused')
{
    // provide defaults for 2nd parameter
    return( get_theme_mod( $key, norfolk_theme_mod_default( $key)));
}



static $norfolk_all_fonts = array(
   'Georgia' => array( 'serif', ''),
   'Arial' => array( 'sans-serif', ''),

   // google
   'Arvo'  => array( 'serif', '400,400i,700,700i'),
   'Cardo'  => array( 'serif', '400,400i,700,700i'),
   'EB Garamond' => array( 'serif', '400,400i,700'),
   'Gentium Basic'  => array( 'serif', '400,400i,700,700i'),
   'Libre Baskerville' => array( 'serif', '400,400i,700'),
   'Neuton' => array( 'serif', '400,400i,700'),

   'Lato'  => array( 'sans-serif', '400,400i,700,700i'),
   'Open Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Work Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Open Sans Condensed'  => array( 'sans-serif', '300'),
   'Raleway'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto Slab'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 static $norfolk_body_text_fonts = array(
   'Georgia' => array( 'serif', ''),
   'Arial' => array( 'Helvetica, sans-serif', ''),

   // google
   'Arvo'  => array( 'serif', '400,400i,700,700i'),
   'EB Garamond' => array( 'serif', '400,400i,700'),
   'Gentium Basic'  => array( 'serif', '400,400i,700,700i'),
   'Libre Baskerville' => array( 'serif', '400,400i,700'),
   'Neuton' => array( 'serif', '400,400i,700'),

   'Lato'  => array( 'sans-serif', '400,400i,700,700i'),
   'Open Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Work Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Raleway'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 // Add Google fonts CSS to admin header
 function norfolk_load_fonts() {

 global $norfolk_all_fonts;

 //<link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
  if ( 0 ) {
     foreach ( $norfolk_all_fonts as $key => $f) {
       if( $f[ 1]) {


         $s = 'https://fonts.googleapis.com/css?family='.str_replace( ' ', '+', $key);
         $s .= ':'.$f[ 1];
        //error_log( $s);
         wp_register_style('google-fonts-'.str_replace( ' ', '-', $key), $s, array(), null);
         wp_enqueue_style( 'google-fonts-'.str_replace( ' ', '-', $key));
       }
     }
   }
 }

 //add_action('customize_controls_enqueue_scripts', 'stout_oak_load_fonts');
 //add_action('wp_enqueue_scripts', 'norfolk_load_fonts');
 add_action('wp_default_scripts', 'norfolk_load_fonts');
 //add_action('admin_enqueue_scripts', 'norfolk_load_fonts');

 // Add Google fonts CSS to admin header
 function norfolk_font_family( $key) {

     global $norfolk_all_fonts;
     return( "'".$key."'".", ".$norfolk_all_fonts[ $key][0]);
}

// return all google fonts used in theme options
function norfolk_get_theme_fonts() {

    $fonts = array();

    $fonts[] = norfolk_get_theme_mod( 'norfolk-font-header');
    $fonts[] = norfolk_get_theme_mod( 'norfolk-font-nav-menu');
    $fonts[] = norfolk_get_theme_mod( 'norfolk-font-heading');
    $fonts[] = norfolk_get_theme_mod( 'norfolk-font-body-text');
    $fonts[] = norfolk_get_theme_mod( 'norfolk-font-button');
    $fonts[] = norfolk_get_theme_mod( 'norfolk-font-caption');
    $fonts[] = norfolk_get_theme_mod( 'norfolk-font-footer');
    $fonts = array_unique( $fonts);

    return( $fonts);
}



function norfolk_google_fonts_filter( $f)
{
//    loco_print_r( $f);
    // remove some fonts
    return( $f['category'] != 'handwriting');
}

function norfolk_get_google_fonts( $max = 5, $all = true)
{
  $googleAllFontList = array();
  global $norfolk_all_fonts;
  global $norfolk_body_text_fonts;

  $fonts = ($all ?  ($norfolk_all_fonts):($norfolk_body_text_fonts));
  foreach( $fonts as $key => $f) {
    $googleAllFontList[ $key] = $key;
  }
  return( $googleAllFontList);

    $standardFontList = array(
            'Andale Mono' => 'andale mono,times',
            'Arial' => 'Arial,sans-serif',
            'Arial Black' => 'arial black,avant garde',
            'Book Antiqua' => 'book antiqua,palatino',
            'Comic Sans MS' => 'comic sans ms,sans-serif',
            'Courier New' => 'courier new,courier',
            'Georgia' => 'georgia,palatino',
            'Helvetica' => 'helvetica',
            'Impact' => 'impact,chicago',
            'Tahoma' => 'tahoma,Arial,sans-serif',
            'Terminal' => 'terminal,monaco',
            'Times New Roman' => 'times new roman,times',
            'Trebuchet MS' => 'trebuchet ms,geneva',
            'Verdana' => 'verdana,geneva',
             );
    $standardFontList = array_flip( $standardFontList);
    return( $standardFontList);

//loco_var_dump( $standardFontList);

    $list = array();

    // Use the Google API to get a list of fonts
    //    https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&key=AIzaSyAdAzzo-XUCV0oBEVkzEx85V24BXkeyimA

    $url = 'https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&fields=items(category%2Cfamily)&key=AIzaSyDjyeFIVeWHP0t95rWVtX7KO2wZUCJ6gFU';
    $response = wp_remote_get( $url);
    if( is_array($response) ) {
      $header = $response['headers']; // array of http header lines
      $body = $response['body']; // use the content
      $b = json_decode( $body);
//      echo "family0:".$b->items[0]->family;
//      echo '<br />';
      $fonts = array();
      foreach ($b->items as $key => $f) {
//          echo "font-family".$key.": ".$f->family. "<br />";
            $fonts[ $key]['family'] = $f->family;
            $fonts[ $key]['category'] = $f->category;
          if ( $key > $max)
            break;
      }
      $fonts =  array_filter( $fonts, 'norfolk_google_fonts_filter');
      sort( $fonts);

//      loco_var_dump( $fonts);
//      loco_print_r( $fonts);
//      loco_print_r( get_theme_mods());

    // key is lowercase for code, value is upper case read-able
    foreach ( $fonts as $key => $f) {
        $k = strtolower( $f['family']).','.strtolower($f['category']);
        $list[ $k] = $f['family'];
    }
//    $list = array_merge($list, $standardFontList);

//      loco_print_r( $list);

    }
//    else
//        echo "response: $response";
    return( $list);
}

function norfolk_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function norfolk_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
 //   $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

    // remove some stuff
    $wp_customize->remove_control( 'header_textcolor' );
    //$wp_customize->remove_panel( 'widgets');
    $wp_customize->remove_section( 'static_front_page');

    ///////////////////////////////////////////////////////////////////////////
    // Colors
    $wp_customize->add_setting( 'norfolk-color-nav-link',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-color-nav-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-color-nav-link',
        array(
        'label' => __( 'Navigation Menu', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'norfolk-color-text-body',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-color-text-body'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-color-text-body',
        array(
        'label' => __( 'Body Text', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'norfolk-color-text-link',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-color-text-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-color-text-link',
        array(
        'label' => __( 'Link', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'norfolk-color-text-footer',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-color-text-footer'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-color-text-footer',
        array(
        'label' => __( 'Footer Text', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'norfolk-color-text-footer-link',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-color-text-footer-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-color-text-footer-link',
        array(
        'label' => __( 'Footer Link', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'norfolk-color-text-footer-rollover',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-color-text-footer-rollover'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-color-text-footer-rollover',
        array(
        'label' => __( 'Footer Link Rollover', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );



    $wp_customize->add_setting( 'norfolk-color-rule-lines',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-color-rule-lines'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-color-rule-lines',
        array(
        'label' => __( 'Rule Lines', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'norfolk-color-call-to-action',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-color-call-to-action'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-color-call-to-action',
        array(
        'label' => __( 'Call to Action Button', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'norfolk-color-background-body',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-color-background-body'),
        'transport' => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-color-background-body',
        array(
        'label' => __( 'Background Body', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );

    /*
    $wp_customize->add_setting( 'norfolk-xxx',
        array(
        'type' => 'theme_mod',
        'default' => norfolk_theme_mod_default( 'norfolk-xxx'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'norfolk-xxx',
        array(
        'label' => __( 'XXX Color', 'norfolk_textdomain' ),
        'section' => 'colors',
    ) ) );
    */


    ///////////////////////////////////////////////////////////////////////////
    // Fonts

    $fontListAll = norfolk_get_google_fonts( 25);
    $fontListBodyText = norfolk_get_google_fonts( 25, false);

    // section
    $wp_customize->add_section( 'norfolk-font-setting', array(
            'title'          => 'Fonts',
            'priority'       => 20,
            'description' => '<b>' . __( 'Select fonts.', 'norfolk' ) .'</b>' ,
        ) );

    // setting/control
    $wp_customize->add_setting( 'norfolk-font-nav-menu', array(
            'type'    => 'theme_mod',
            'default' => norfolk_theme_mod_default('norfolk-font-nav-menu'),
//            'transport' => 'postMessage',
            'transport' => 'refresh',
        ) );

    $wp_customize->add_control( 'norfolk-font-nav-menu', array(
            'label' => 'Navigation Menu',
            'section' => 'norfolk-font-setting',
            'settings'   => 'norfolk-font-nav-menu',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );


    $wp_customize->add_setting( 'norfolk-font-heading', array(
            'type'    => 'theme_mod',
            'default' => norfolk_theme_mod_default('norfolk-font-heading'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'norfolk-font-heading', array(
            'label' => 'Heading',
            'section' => 'norfolk-font-setting',
            'settings'   => 'norfolk-font-heading',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'norfolk-font-body-text', array(
            'type'    => 'theme_mod',
            'default' => norfolk_theme_mod_default('norfolk-font-body-text'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'norfolk-font-body-text', array(
            'label' => 'Body Text',
            'section' => 'norfolk-font-setting',
            'settings'   => 'norfolk-font-body-text',
            'type'    => 'select',
            'choices' => $fontListBodyText,
    ) );

    $wp_customize->add_setting( 'norfolk-font-button', array(
            'type'    => 'theme_mod',
            'default' => norfolk_theme_mod_default('norfolk-font-button'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'norfolk-font-button', array(
            'label' => 'Button',
            'section' => 'norfolk-font-setting',
            'settings'   => 'norfolk-font-button',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'norfolk-font-caption', array(
            'type'    => 'theme_mod',
            'default' => norfolk_theme_mod_default('norfolk-font-caption'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'norfolk-font-caption', array(
            'label' => 'Caption',
            'section' => 'norfolk-font-setting',
            'settings'   => 'norfolk-font-caption',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'norfolk-font-footer', array(
            'type'    => 'theme_mod',
            'default' => norfolk_theme_mod_default('norfolk-font-footer'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'norfolk-font-footer', array(
            'label' => 'Footer',
            'section' => 'norfolk-font-setting',
            'settings'   => 'norfolk-font-footer',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );


/*
    $wp_customize->add_setting( 'norfolk-font-xxx', array(
            'type'    => 'theme_mod',
            'default' => 'Arial',
            'transport' => 'postMessage',
    ) );

    $wp_customize->add_control( 'norfolk-font-xxx', array(
            'label' => 'Button Font',
            'section' => 'norfolk-font-setting',
            'settings'   => 'norfolk-font-xxx',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

*/


    ///////////////////////////////////////////////////////////////////////////
    // Social Media
    $wp_customize->add_section( 'norfolk-social-media-settings', array(
        'title'          => 'Social Media Settings',
        'priority'       => 160,
        'description' => '<b>' . __( 'Social media links.' ) .'</b>' ,
    ) );

    $wp_customize->add_setting( 'norfolk-company-name', array(
            'type'    => 'theme_mod',
            'default' => 'test',
            'transport' => 'postMessage',
            'sanitize_callback' => 'norfolk_sanitize_text',
        ) );
    $wp_customize->add_control( 'norfolk-company-name', array(
            'label' => 'Company Name',
            'section' => 'norfolk-copyright-message-settings',
            'settings'   => 'norfolk-company-name',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'norfolk-facebook', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'norfolk_sanitize_text',
    ) );
    $wp_customize->add_control( 'norfolk-facebook', array(
        'label' => __( 'Facebook URL', 'loco_textdomain' ),
        'section' => 'norfolk-social-media-settings',
        'settings'   => 'norfolk-facebook',
        'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'norfolk-instagram', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'norfolk_sanitize_text',
    ) );
    $wp_customize->add_control( 'norfolk-instagram', array(
        'label' => __( 'Instagram URL', 'loco_textdomain' ),
        'section' => 'norfolk-social-media-settings',
        'settings'   => 'norfolk-instagram',
        'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'norfolk-tripadvisor', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'norfolk_sanitize_text',
    ) );
    $wp_customize->add_control( 'norfolk-tripadvisor', array(
        'label' => __( 'Trip Advisor URL', 'loco_textdomain' ),
        'section' => 'norfolk-social-media-settings',
        'settings'   => 'norfolk-tripadvisor',
        'type'    => 'text',
    ) );
    $wp_customize->add_setting( 'norfolk-yelp', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'norfolk_sanitize_text',
    ) );
    $wp_customize->add_control( 'norfolk-yelp', array(
        'label' => __( 'Yelp URL', 'loco_textdomain' ),
        'section' => 'norfolk-social-media-settings',
        'settings'   => 'norfolk-yelp',
        'type'    => 'text',
    ) );


    ///////////////////////////////////////////////////////////////////////////
    // Footer

    $wp_customize->add_section( 'norfolk-copyright-message-settings', array(
            'title'          => 'Contact Information',
            'priority'       => 200,
    ) );


    $wp_customize->add_setting( 'norfolk-address', array(
            'type'    => 'theme_mod',
            'default' => '',
            'transport' => 'postMessage',
            'sanitize_callback' => 'norfolk_sanitize_text',
        ) );
    $wp_customize->add_control( 'norfolk-address', array(
            'label' => 'Address',
            'section' => 'norfolk-copyright-message-settings',
            'settings'   => 'norfolk-address',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'norfolk-telephone', array(
            'type'    => 'theme_mod',
            'default' => norfolk_theme_mod_default('norfolk-telephone'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'norfolk_sanitize_text',
    ) );
    $wp_customize->add_control( 'norfolk-telephone', array(
            'label' => 'Telephone',
            'section' => 'norfolk-copyright-message-settings',
            'settings'   => 'norfolk-telephone',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'norfolk-copyright-message', array(
            'type'    => 'theme_mod',
            'default' => '© Copyright 2015. All rights reserved.',
            'transport' => 'postMessage',
            'sanitize_callback' => 'norfolk_sanitize_text',
        ) );
    $wp_customize->add_control( 'norfolk-copyright-message', array(
            'label' => 'Copyright Text',
            'section' => 'norfolk-copyright-message-settings',
            'settings'   => 'norfolk-copyright-message',
            'type'    => 'text',
    ) );


}
add_action( 'customize_register', 'norfolk_customize_register' );


function norfolk_customize_css()
{
  // add customizer CSS to <head>
  ?>
    <style type="text/css">

      .navbar-default .navbar-nav > .open  > a,
        .navbar-default .navbar-nav > .open  > a:focus,
        .navbar-default .navbar-nav > .open  > a:hover,
        .navbar-default .navbar-nav .open .dropdown-menu > li > a,
        .navbar-default .navbar-nav .dropdown-menu > li > a,
         {
          color: <?php echo norfolk_get_theme_mod( 'norfolk-color-nav-link', '#fff' ); ?>;
          font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-nav-menu', 'Arial')); ?>;
        }

        .navbar-default .navbar-nav  li > a {
          color: <?php echo norfolk_get_theme_mod( 'norfolk-color-nav-link', '#fff' ); ?>;
          font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-nav-menu', 'Arial')); ?>;
        }
        input[type="submit"] {
            background-color: <?php echo norfolk_get_theme_mod( 'norfolk-color-text-link', '#ccc' ); ?>;
            border-color: <?php echo norfolk_get_theme_mod( 'norfolk-color-call-to-action', '#fff' ); ?>;
            font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-button', 'Arial')); ?>;
        }

        .navbar-default .navbar-nav > li > a:hover {
            color: <?php echo norfolk_get_theme_mod( 'norfolk-color-text-link', '#aaa' ); ?>;
        }

        .navbar-default .navbar-nav .current-menu-item a {
            color: <?php echo norfolk_get_theme_mod( 'norfolk-color-nav-link', '#aaa' ); ?>;
            /*border-bottom: 2px solid <?php echo norfolk_get_theme_mod( 'norfolk-color-text-link', '#aaa' ); ?>;*/
        }

        .navbar .navbar-header button .icon-bar {
          background-color: <?php echo norfolk_get_theme_mod( 'norfolk-color-text-link', '#aaa' ); ?>;
        }
        .navbar .navbar-header button .icon-title {
          color: <?php echo norfolk_get_theme_mod( 'norfolk-color-text-link', '#aaa' ); ?>;
        }


        body {
            background-color: <?php echo norfolk_get_theme_mod( 'norfolk-color-background-body', '#fff' ); ?>;
        }

        body, #main {
            color: <?php echo norfolk_get_theme_mod( 'norfolk-color-text-body', '#000' ); ?>;
            font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-body-text', 'Arial')); ?>;
        }

        .site-header, .site-main {
          font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-body-text', 'Arial')); ?>;
        }

        a, a:visited, a:hover, .site-main a, .site-main a:visited, .site-main a:active, .site-main a:hover, .site-main a:focus {
          color: <?php echo norfolk_get_theme_mod( 'norfolk-color-text-link', '#aaa' ); ?>;
        }
        .page .container .call-to-action {
          background-color: <?php echo norfolk_get_theme_mod( 'norfolk-color-call-to-action'); ?>;
        }
        h1, h2, h3, h4, h5, h6 {
            font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-heading', 'Arial')); ?>;
        }
        .page .hero h1, .page .hero h2 {
            font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-title', 'Arial')); ?>;
        }
        hr {
          border-color: <?php echo norfolk_get_theme_mod( 'norfolk-color-rule-lines', '#aaa' ); ?>;
        }
        /*.container, .content-area, .site-main, .widget-area, .site-content  {
            background-color: <?php echo norfolk_get_theme_mod( 'norfolk-body-backgroundcolor', '#fff' ); ?>;
        }*/

        .button {
            font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-button', 'Arial')); ?>;
        }

        .caption {
            font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-caption', 'Arial')); ?>;
        }
        .wp-caption {
          font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-caption', 'Arial')); ?>;
        }

        #secondary {
        }

        .site-footer {
            /*background-color: <?php echo norfolk_get_theme_mod( 'norfolk-footer-backgroundcolor', '#ddd' ); ?>;*/
            color: <?php echo norfolk_get_theme_mod( 'norfolk-color-text-footer'); ?>;
        }

        .site-footer a, .site-footer a .fa {
          color: <?php echo norfolk_get_theme_mod( 'norfolk-color-text-footer-link'); ?>;
        }

        .site-footer a:focus, .site-footer a:hover, .site-footer a:active,
        .site-footer a .fa:focus, .site-footer a .fa:hover, .site-footer a .fa:active {
          color: <?php echo norfolk_get_theme_mod( 'norfolk-color-text-footer-rollover'); ?>;
        }

        .site-footer {
            font-family: <?php echo norfolk_font_family( norfolk_get_theme_mod( 'norfolk-font-footer', 'Arial')); ?>;
        }
    </style>


    <?php

    // enqueue customizer external CSS (fonts, etc.)
    // fonts
    $key = get_theme_mod( 'norfolk-font-text', 'Arial');
    //$fontList = norfolk_get_google_fonts( 25);
    //$family = $fontList[ $key];

    //$s = 'http://fonts.googleapis.com/css?family='.$family.':400';
    //$s = str_replace( ' ', '+', $s );
//    wp_register_style('googleFonts_norfolk-font-text', $s, array(), null);
//    wp_enqueue_style( 'googleFonts_norfolk-font-text');

}
add_action( 'wp_head', 'norfolk_customize_css');

// change editor_style CSS dynamically for the admin editor
add_filter('tiny_mce_before_init','norfolk_theme_editor_dynamic_styles');
function norfolk_theme_editor_dynamic_styles( $mceInit ) {
    $styles = " \
    body.mce-content-body {  \
      background-color: ".norfolk_get_theme_mod( 'norfolk-body-backgroundcolor', '#fff' )."; \
      color:".norfolk_get_theme_mod( 'norfolk-body-textcolor', '#000' )."; \
      font-family:".norfolk_get_theme_mod( 'norfolk-font-body-text', 'Arial')."; \
      font-size: 14px; \
    } \
    body.mce-content-body h1, h2, h3, h4, h5, h6 { \
      font-family:".norfolk_get_theme_mod( 'norfolk-font-heading-text', 'Arial')."; \
    } \
    .wp-caption-dd { \
      color:".norfolk_get_theme_mod( 'norfolk-body-textcolor', '#000' )."; \
    } \
    ";

    if ( isset( $mceInit['content_style'] ) ) {
        $mceInit['content_style'] .= ' ' . $styles . ' ';
    } else {
        $mceInit['content_style'] = $styles . ' ';
    }
    return $mceInit;
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function norfolk_customize_preview_js() {
    wp_enqueue_script( 'norfolk_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '4'.date( '.YmdGi'), true );
}

add_action( 'customize_preview_init', 'norfolk_customize_preview_js' );
