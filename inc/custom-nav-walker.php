<?php


class Nav_MainMenu_Walker extends Walker_Nav_Menu {

  function start_el( &$output, $item, $depth=0, $args=array(), $id = 0) {
    global $current_menu_item_parent;

    //the_post();
    // get html plage classes
    $classes =  get_body_class();
//var_dump( $classes);

    // top level
//var_dump( $item);
//var_dump( $item->title);
    if ( $item->menu_item_parent == '0' &&
      ((true === $item->current_item_parent ||
      ( in_array( 'menu-item-has-children', $item->classes) && in_array( 'current-menu-item', $item->classes ))) ||
      (( in_array( 'blog', $classes) || in_array( 'single-post',  $classes) ) && in_array( 'has-blog-post', $item->classes )) ||
      (( in_array( 'post-type-archive-event', $classes) || in_array( 'single-event',  $classes) ) && in_array( 'has-event', $item->classes )) ||
      (( in_array( 'post-type-archive-exhibition', $classes) || in_array( 'single-exhibition',  $classes) ) && in_array( 'has-exhibition', $item->classes )) ||
      (( in_array( 'post-type-archive-collection', $classes) || in_array( 'single-collection',  $classes) ) && in_array( 'has-collection', $item->classes ))
      )) {
      $current_menu_item_parent = strval( $item->ID);
      $item->classes[] = 'current-menu-parent';
    }



    //var_dump( $current_menu_item_parent);
    //var_dump( $item->menu_item_parent);

        // one level down
        if ( $depth == 0  ) {

          $object = $item->object;
        	$type = $item->type;
        	$title = $item->title;
        	$description = $item->description;
        	$permalink = $item->url;
          $classes = implode(" ", $item->classes);
          $output .= "<li class='" .  $classes . "'>";
          //$output .= '<a href="' . $permalink . '">';

          //Add SPAN if no Permalink
          if( ( $permalink && $permalink != '#' ) || ( in_array( 'search-glass', $item->classes ))) {
          	$output .= '<a href="' . $permalink . '">';
          } else {
          	$output .= '<span>';
          }

          $output .= $title;

          if( ( $permalink && $permalink != '#' ) || ( in_array( 'search-glass', $item->classes ))) {
          	$output .= '</a>';
          } else {
          	$output .= '</li>';
          }

          $output .= "</li>";
        }


  }

}


$current_menu_item_parent = "";

class Nav_SubMenu_Walker extends Walker_Nav_Menu {


  function start_el( &$output, $item, $depth=0, $args=array(), $id = 0) {
    global $current_menu_item_parent;

    //the_post();
    // get html plage classes
    $classes =  get_body_class();
//var_dump( $classes);

    // top level
//var_dump( $item);
//var_dump( $item->title);
    if ( $item->menu_item_parent == '0' &&
      ((true === $item->current_item_parent ||
      ( in_array( 'menu-item-has-children', $item->classes) && in_array( 'current-menu-item', $item->classes ))) ||
      (( in_array( 'blog', $classes) || in_array( 'single-post',  $classes) ) && in_array( 'has-blog-post', $item->classes )) ||
      (( in_array( 'post-type-archive-event', $classes) || in_array( 'single-event',  $classes) ) && in_array( 'has-event', $item->classes )) ||
      (( in_array( 'post-type-archive-exhibition', $classes) || in_array( 'single-exhibition',  $classes) ) && in_array( 'has-exhibition', $item->classes )) ||
      (( in_array( 'post-type-archive-collection', $classes) || in_array( 'single-collection',  $classes) ) && in_array( 'has-collection', $item->classes ))
      )) {
      $current_menu_item_parent = strval( $item->ID);
      $item->classes[] = 'current-menu-parent';
      $item->classes[] = 'current_page_parent';

//current-menu-ancestor.current-menu-parent.current_page_parent.current_page_ancestor.menu-item-has-children.menu-item-1955

//var_dump( $item->classes);
//var_dump( $output);
    }

//var_dump( $current_menu_item_parent);
//var_dump( $item->menu_item_parent);

    // one level down
    if ( 1 === $depth && $item->menu_item_parent == $current_menu_item_parent ) {

      if (
        (( in_array( 'single-post', $classes ) || in_array( 'blog', $classes )) && in_array( 'has-blog-post', $item->classes)) ||
        (( in_array( 'post-type-archive-event', $classes) || in_array( 'single-event',  $classes)) && in_array( 'has-event', $item->classes)) ||
        (( in_array( 'post-type-archive-exhibition', $classes) || in_array( 'single-exhibition',  $classes) ) && in_array( 'has-exhibition', $item->classes )) ||
        (( in_array( 'post-type-archive-collection', $classes) || in_array( 'single-collection',  $classes) ) && in_array( 'has-collection', $item->classes ))
        ) {
        $item->classes[] = 'current-menu-item';
//var_dump( $item->classes);

      }

      $object = $item->object;
    	$type = $item->type;
    	$title = $item->title;
    	$description = $item->description;
    	$permalink = $item->url;
      $classes = implode(" ", $item->classes);
      $output .= "<li class='" .  $classes . "'>";

      //Add SPAN if no Permalink
      if( $permalink && $permalink != '#' ) {
      	$output .= '<a href="' . $permalink . '">';
      } else {
      	$output .= '<span>';
      }

      $output .= $title;

      if( $permalink && $permalink != '#' ) {
      	$output .= '</a>';
      } else {
      	$output .= '</li>';
      }

      $output .= "</li>";
    }
  }

  function end_el( &$output, $item, $depth=0, $args=array(), $id = 0) {
    if ( 1 == $depth)
      $output .= ' ';
  }


  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n";
  }

	function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n";
  }

}

?>
