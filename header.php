<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package norfolk
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'norfolk' ); ?></a>

<div class="container header">
	<header id="masthead" class="site-header row" role="banner">
		<div class="site-branding col-xs-12">

			<?php	// Try to retrieve the Custom Logo
				if ( has_custom_logo()): ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="home" rel="home"><?php the_custom_logo(); ?></a>
			<?php endif; ?>

			<?php
			// Try to retrieve the Custom Logo
	    $output = '';
	    if (function_exists('get_custom_logo'))
	        $output = get_custom_logo();
	    // Nothing in the output: Custom Logo is not supported, or there is no selected logo
	    // In both cases we display the site's name
	    if (empty($output))
				$output = '<a href="' . esc_url(home_url('/')) . '"><h1>' . get_bloginfo('name') . '</h1>';
//	      $output .= '<h2>13 Village Green, Norfolk Connecticut 06058</h2></a>';

//	    echo $output;
			?>

		</div><!-- .site-branding -->

		<nav class="navbar navbar-default col-xs-12" role="navigation">

			<!-- mobile and desktop -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-title">Menu</span>
				</button>
				<span class="search-glass-mobile visible-xs visible-sm"><a><i class="fa fa-search"></i></a></span>
			</div>

			<!--- mobile -->
			<div class="visible-xs visible-sm">
				<?php
				$menu_args = array(
				"theme_location" => "primary",
				"container_class" => "navbar-collapse collapse",
				"menu_class" => "nav navbar-nav navbar-mobile",
				"menu_id" => "main-primary-menu",
				);
				wp_nav_menu( $menu_args);
				?>
			</div>

			<!-- desktop -->
			<div class="visible-md visible-lg">
				<?php
				$menu_args = array(
				"theme_location" => "primary",
				"container_class" => "navbar-collapse collapse",
				"menu_class" => "nav navbar-nav",
				"menu_id" => "main-primary-menu",
				"depth" => 1,
				"walker" => new Nav_MainMenu_Walker(),
				);
				wp_nav_menu( $menu_args);
				?>

				<!-- sub-menu display -->
				<?php
				$menu_args = array(
				"theme_location" => "primary",
				"container_class" => "navbar-collapse collapse",
				"menu_class" => "nav navbar-nav",
				"menu_id" => "main-primary-sub-menu",
				"walker" => new Nav_SubMenu_Walker(),
				);
				wp_nav_menu( $menu_args);
				?>
			</div>

		</nav>

		<div class="col-xs-12 text-left search-mobile" style="display:none;">
			<?php get_search_form( true ); ?>
		</div>

		<div class="col-sm-12 search-desktop" style="display:none;">
			<?php echo get_search_form();?>
		</div>

	</header>
</div><!-- end container -->

<div class="container content">
	<div id="content" class="site-content row">
