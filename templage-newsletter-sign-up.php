<?php
/*
 * Template Name: Newsletter Sign Up
 *
 * @package norfolk
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12" role="main">

			<!--Begin CTCT Sign-Up Form-->
			<!-- EFD 1.0.0 [Tue May 23 12:09:20 EDT 2017] -->
			<link rel='stylesheet' type='text/css' href='https://static.ctctcdn.com/h/contacts-embedded-signup-assets/1.0.2/css/signup-form.css'>
			<div class="ctct-embed-signup" style="font: 16px Helvetica Neue, Arial, sans-serif; font: Helvetica Neue, Arial, sans-serif; line-height: 1.5; -webkit-font-smoothing: antialiased;">
			   <div style="color:#5b5b5b; background-color:#EFE9E5; border-radius:5px;">
			       <span id="success_message" style="display:none;">
			           <div style="text-align:center;">Thanks for signing up!</div>
			       </span>
			       <form data-id="embedded_signup:form" class="ctct-custom-form Form" name="embedded_signup" method="POST" action="https://visitor2.constantcontact.com/api/signup">
			           <h2 style="margin:0;">Email Newsletter</h2>
			           <p>Sign up to receive our email newsletter.</p>
			           <!-- The following code must be included to ensure your sign-up form works properly. -->
			           <input data-id="ca:input" type="hidden" name="ca" value="8d13237a-f529-4aab-b007-a5db57d3e824">
			           <input data-id="list:input" type="hidden" name="list" value="1903660269">
			           <input data-id="source:input" type="hidden" name="source" value="EFD">
			           <input data-id="required:input" type="hidden" name="required" value="list,email">
			           <input data-id="url:input" type="hidden" name="url" value="">
			           <p data-id="Email Address:p" ><label data-id="Email Address:label" data-name="email" class="ctct-form-required">Email Address</label> <input data-id="Email Address:input" type="text" name="email" value="" maxlength="80"></p>
			           <p data-id="First Name:p" ><label data-id="First Name:label" data-name="first_name">First Name</label> <input data-id="First Name:input" type="text" name="first_name" value="" maxlength="50"></p>
			           <p data-id="Last Name:p" ><label data-id="Last Name:label" data-name="last_name">Last Name</label> <input data-id="Last Name:input" type="text" name="last_name" value="" maxlength="50"></p>
			           <button type="submit" class="Button ctct-button Button--block Button-secondary" data-enabled="enabled">Sign Up</button>
			       	<div><p class="ctct-form-footer">By submitting this form, you are granting: Norfolk Historical Society, 13 Village Green, Norfolk, Connecticut, 06058, United States,  permission to email you. You may unsubscribe via the link found at the bottom of every email.  (See our <a href="http://www.constantcontact.com/legal/privacy-statement" target="_blank">Email Privacy Policy</a> for details.) Emails are serviced by Constant Contact.</p></div>
			       </form>
			   </div>
			</div>
			<script type='text/javascript'>
			   var localizedErrMap = {};
			   localizedErrMap['required'] = 		'This field is required.';
			   localizedErrMap['ca'] = 			'An unexpected error occurred while attempting to send email.';
			   localizedErrMap['email'] = 			'Please enter your email address in name@email.com format.';
			   localizedErrMap['birthday'] = 		'Please enter birthday in MM/DD format.';
			   localizedErrMap['anniversary'] = 	'Please enter anniversary in MM/DD/YYYY format.';
			   localizedErrMap['custom_date'] = 	'Please enter this date in MM/DD/YYYY format.';
			   localizedErrMap['list'] = 			'Please select at least one email list.';
			   localizedErrMap['generic'] = 		'This field is invalid.';
			   localizedErrMap['shared'] = 		'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
			   localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
				localizedErrMap['state_province'] = 'Select a state/province';
			   localizedErrMap['selectcountry'] = 	'Select a country';
			   var postURL = 'https://visitor2.constantcontact.com/api/signup';
			</script>
			<script type='text/javascript' src='https://static.ctctcdn.com/h/contacts-embedded-signup-assets/1.0.2/js/signup-form.js'></script>
			<!--End CTCT Sign-Up Form-->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
