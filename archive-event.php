<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package norfolk
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					//the_archive_title( '<h1 class="page-title">', '</h1>' );
					//the_archive_description( '<div class="archive-description">', '</div>' );
				?>
				<h2>EVENTS</h2>
			</header><!-- .page-header -->

			<?php
					global $query_string;
					//query_posts( $query_string . '&orderby=menu_order&order=ASC&posts_per_page=-1' );
					$args =  array(
							'post_type' => 'event',
							'orderby' => array(
									'menu_order' => 'ASC',
									'date' => 'DESC',
							),
							'posts_per_page' => '-1',
					);
					query_posts( $args);
			?>
			<?php

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				 get_template_part( 'template-parts/content-event');

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
