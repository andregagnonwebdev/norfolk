<?php
/**
 * Template part for displaying exhibitions.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package norfolk
 */

?>

<?php if ( !is_single() ) : ?>
	<hr  />
<?php endif; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="row">

		<header class="entry-header col-xs-12">
			<?php
			if ( is_single() ) :
				the_title( '<h2 class="entry-title">', '</h2>' );
			//else :
			//	the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
			?>
		</header><!-- .entry-header -->

	</div>

	<div class="row">
		<div class="entry-content col-xs-12 col-sm-4 col-lg-3">
			<!-- featured image -->
			<?php if ( has_post_thumbnail() ) : ?>
				<div class="featured-image">
						<a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>">
							<?php the_post_thumbnail('medium'); ?>
				    </a>
				</div>
 			<?php endif; ?>

			<!-- old image, caption -->
			<?php if ( $f = get_field( 'old_image') ) : ?>
				<div class="featured-image">
						<a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>">
							<img src="<?php echo $f ?>" />
						</a>
				</div>
			<?php endif; ?>


		</div>

		<div class="entry-content content col-xs-12 col-sm-8 col-lg-9">
			<?php if ( is_single() ) : ?>
				<?php //the_title( '<h3 class="entry-title">', '</h3>' ); ?>
			<?php else: ?>
				<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
			<?php endif; ?>

				<!--  date, time -->
				<?php if ( $f = get_field( 'date') ) : ?>
					<?php echo $f; ?>
				<?php elseif ( $y = get_field( 'old_year') ) : ?>
					<?php $m = get_field( 'old_month'); ?>
					<?php $d = get_field( 'old_day'); ?>
					<?php echo $m.'/'.$d.'/'.$y; ?>
				<?php endif; ?>
				<?php if ( $f = get_field( 'time') ) : ?>
					<?php echo ' - '.$f; ?>
				<?php elseif ( $f = get_field( 'old_time') ) : ?>
					<?php echo ' - '.$f; ?>
				<?php endif; ?>

				<!-- location -->
				<?php if ( 1 || is_single() ) : ?>
					<p><?php echo get_field( 'location'); ?></p>
				<?php endif; ?>


				<?php
				//the_excerpt( sprintf(
					the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'norfolk' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Exhibitions:', 'norfolk' ),
						'after'  => '</div>',
					) );
				?>
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<?php //norfolk_entry_footer(); ?>
		</footer><!-- .entry-footer -->

	</div>

</article><!-- #post-## -->
