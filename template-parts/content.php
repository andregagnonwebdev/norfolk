<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package norfolk
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<header class="entry-header col-xs-12">
			<?php
			if ( is_single() ) :
				the_title( '<h2 class="entry-title">', '</h2>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php norfolk_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php
			endif; ?>
		</header><!-- .entry-header -->
	</div>

	<div class="row">
		<div class="entry-content col-xs-12 col-sm-4 col-lg-3">
			<!-- featured image -->
			<?php if ( has_post_thumbnail() ) : ?>
				<div class="featured-image">
						<a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>">
				      <?php the_post_thumbnail('thumbnail'); ?>
				    </a>
				</div>
 			<?php endif; ?>


			<!-- subtitle -->
			<?php if ( is_single() ) : ?>
				<h3><?php echo get_field( 'subtitle'); ?></h3>
			<?php endif; ?>
		</div>

		<div class="entry-content col-xs-12 col-sm-8 col-lg-9">
			<?php if ( is_single() ) : ?>
				<?php
				//the_excerpt( sprintf(
					the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'norfolk' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Exhibitions:', 'norfolk' ),
						'after'  => '</div>',
					) );
				?>
			<?php else: ?>
				<div style="margin-top:-20px;">
					<h3><?php echo get_field( 'subtitle'); ?></h3>
					<?php the_excerpt(); ?>
					</div>
			<?php endif; ?>
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<?php //norfolk_entry_footer(); ?>
		</footer><!-- .entry-footer -->

	</div>
</article><!-- #post-## -->
