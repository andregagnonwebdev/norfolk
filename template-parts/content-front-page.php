<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package norfolk
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!-- <header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header> -->
	<!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();  // slider embbeded in content
		?>

		<?php if( have_rows('cards') ):	?>

			<div class="cards">

					<?php
				 	// loop through the rows of data
						$cards = array();
				    while ( have_rows('cards') ) : the_row();
						        // display a sub field value
										$l = get_sub_field( 'link');
										$l = get_sub_field( 'link_url');
										$t = get_sub_field( 'link_title');
										$c = array();
										$c['image'] = get_sub_field( 'image');
										$o = get_sub_field_object( 'image');
										$c['link_url']= get_sub_field( 'link_url');
										$c['link_title'] = get_sub_field( 'link_title');

										// get custom image
										$i = get_sub_field( 'image');
										$c['alt'] = $i['alt'];
										$size = 'home-card';
										$c['thumb'] = $i['sizes'][ $size ];
										$c['width'] = $i['sizes'][ $size . '-width' ];
										$c['height'] = $i['sizes'][ $size . '-height' ];

										$cards[] = $c;

				    endwhile;
						?>

						<div class="row visible-xs">
						<?php foreach ( $cards as $c): ?>
							<div class="card col-xs-12">
								<a href="<?php echo $c['link_url']; ?>"><?php echo $c['link_title']; ?></a>
								<br  />
								<a href="<?php echo $c['link_url']; ?>">
									<!-- <img class="" src="<?php echo $c['image']; ?>" /> -->
									<img classx="img-responsive" src="<?php echo $c['thumb']; ?>" alt="<?php echo $c['alt']; ?>" width="<?php echo $c['width']; ?>" height="<?php echo $c['height']; ?>" />
								</a>
								<br  />
							</div>
						<?php	endforeach; ?>
						</div>

						<div class="row hidden-xs">
						<?php foreach ( $cards as $c): ?>
							<div class="card col-xs-12 col-sm-6 col-md-3">
								<a href="<?php echo $c['link_url']; ?>"><?php echo $c['link_title']; ?></a>
								<br  />
							</div>
						<?php	endforeach; ?>
						</div>

						<div class="row hidden-xs">
						<?php foreach ( $cards as $c): ?>
							<div class="card col-xs-12 col-sm-6 col-md-3">
								<a href="<?php echo $c['link_url']; ?>">
									<!-- <img class="" src="<?php echo $c['image']; ?>" /> -->
									<img class="img-responsive" src="<?php echo $c['thumb']; ?>" alt="<?php echo $c['alt']; ?>" width="<?php echo $c['width']; ?>" height="<?php echo $c['height']; ?>" />
								</a>
								<br  />
							</div>
						<?php	endforeach; ?>
						</div>

			</div>
		<?php	endif;?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
