<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package norfolk
 */

?>

	</div><!-- #content -->
</div> <!-- end container -->

<div class="container footer">
	<div id="content" class="site-content row">
		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info  col-xs-12 col-sm-8">

				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navigation" title="home" rel="home">

					<?php	// Try to retrieve the Custom Logo
						if ( 0 && has_custom_logo()): ?>
							<?php the_custom_logo(); ?>
					<?php else: ?>
							<?php echo strtoupper( get_bloginfo('name')); ?>
					<?php endif; ?>
				</a>


				<!-- <a href="/email-newsletter-sign-up"><p>Email Newsletter Sign Up</p></a> -->
				<p>
					<a href="<?php echo esc_url( home_url( '/email-newsletter-sign-up' ) ); ?>" class="" title="" rel="">Email Newsletter Sign Up</a>
					<br /><a href="<?php echo esc_url( home_url( '/donate' ) ); ?>" class="" title="" rel="">Donate</a>
				</p>

					<?php if ( $s = norfolk_get_theme_mod( 'norfolk-address')): ?>
						<div class="address"><?php echo $s ?></div>
					<?php endif; ?>
					<?php if ( $s = norfolk_get_theme_mod( 'norfolk-telephone')): ?>
						<div class="telephone"><?php echo $s ?></div>
					<?php endif; ?>

			</div><!-- .site-info -->
			<div class="site-info social col-xs-12 col-sm-4">
				<?php if ( $s = norfolk_get_theme_mod( 'norfolk-facebook')): ?>
					<a href="<?php echo $s ?>" target="_blank" title="facebook"><i class="fa fa-facebook"></i></a>
				<?php endif; ?>
				<?php if ( $s = norfolk_get_theme_mod( 'norfolk-instagram')): ?>
					&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo $s ?>" target="_blank" title="instagram"><i class="fa fa-instagram"></i></a>
				<?php endif; ?>
				<?php if ( $s = norfolk_get_theme_mod( 'norfolk-tripadvisor')): ?>
					&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo $s ?>" target="_blank" title="tripadvisor"><i class="fa fa-tripadvisor"></i></a>
				<?php endif; ?>
				<?php if ( $s = norfolk_get_theme_mod( 'norfolk-yelp')): ?>
					&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo $s ?>" target="_blank" title="yelp"><i class="fa fa-yelp"></i></a>
				<?php endif; ?>
				&nbsp;<br />
			</div>

			<div class="site-info copyright col-xs-12 col-sm-12">
				<?php if ( $s = norfolk_get_theme_mod( 'norfolk-copyright-message')): ?>
					<span><?php echo $s ?></span>
				<?php else: ?>
					<span>© 2017 All Rights Reserved</span>
				<?php endif; ?>

			</div><!-- .site-info -->
			<!-- <div class="site-infox copyrightx col-xs-12 col-sm-12 text-right">
				<span style="font-size:12px;">Website by <a href="http://andregagnon.com">Andre Gagnon</a></span>
			</div> -->

		</footer><!-- #colophon -->
	</div>
</div> <!-- end container -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
