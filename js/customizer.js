/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	// main background color.
	wp.customize( 'norfolk-tophat-backgroundcolor', function( value ) {
		value.bind( function( to ) {
				$( '.norfolk-header-nav, .norfolk-header-nav .container ' ).css( {
					'backgroundColor': to,
				} );
		} );
	} );

	// menu
	wp.customize( 'norfolk-menu-backgroundcolor', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default' ).css( {
					'backgroundColor': to,
				} );
		} );
	} );
	wp.customize( 'norfolk-menu-buttoncolor', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li').css( {
					'backgroundColor': to,
				} );
		} );
	} );	
	wp.customize( 'norfolk-menu-textcolor', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li > a' ).css( {
					'color': to
				} );
				$( '.navbar-default' ).css( {
//					'border-color': to
				} );
				$( '.navbar-default .navbar-toggle .icon-bar' ).css( {
					'background-color': to
				} );					
		} );
	} );
	wp.customize( 'norfolk-menu-hovercolor', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li:hover' ).css( {
					'backgroundColor': to
				} );
		} );
	} );
	wp.customize( 'norfolk-menu-hover-textcolor', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li > a:hover' ).css( {
					'color': to
				} );
		} );
	} );
//	var id = wp.customize.( 'menu_hovercolor' ).section();
//	console.log( wp.customize( 'menu_hovercolor', function( value ){}) );

	wp.customize( 'norfolk-menu-hovercolor', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li' ).hover( 
					function() {
						$( this).css( {'backgroundColor': to})
					},
					function() {
						$( this).css( { 'backgroundColor': 'transparent'})
					}
				);
		} );
	} );

	wp.customize( 'norfolk-body-textcolor', function( value ) {
		value.bind( function( to ) {
				$( '#main' ).css( {
					'color': to
				} );
		} );
	} );

	wp.customize( 'norfolk-sidebar-textcolor', function( value ) {
		value.bind( function( to ) {
				$( '#secondary' ).css( {
					'color': to
				} );
		} );
	} );
	wp.customize( 'norfolk-footer-textcolor', function( value ) {
		value.bind( function( to ) {
				$( '.site-footer, .site-footer .fa' ).css( { 'color': to	} );
				$( '.site-footer a' ).css( { 'color': to } );
		} );
	} );


	// main background color.
	wp.customize( 'norfolk-body-backgroundcolor', function( value ) {
		value.bind( function( to ) {
				$( '#main' ).css( {
					'backgroundColor': to,
				} );
				$( '.container, .content-area, .site-main, .site-content' ).css( {
					'backgroundColor': to,
				} );
		} );
	} );
	// main background color.
	wp.customize( 'norfolk-sidebar-backgroundcolor', function( value ) {
		value.bind( function( to ) {
				$( '#secondary' ).css( {
					'backgroundColor': to,
				} );
		} );
	} );
	// main background color.
	wp.customize( 'norfolk-footer-backgroundcolor', function( value ) {
		value.bind( function( to ) {
				$( '.site-footer, .norfolk-footer-nav, .norfolk-footer-nav .container ' ).css( {
					'backgroundColor': to,
				} );
		} );
	} );

	function norfolk_bind_html ( theme_mod) {
		wp.customize( theme_mod, function( value ) {
			value.bind( function( to ) {
				var class_name = '.' + theme_mod;
				$( class_name ).html( to );
			} );
		} );
	}

	function norfolk_bind_href ( theme_mod) {
		wp.customize( theme_mod, function( value ) {
			value.bind( function( to ) {
				var class_name = 'a.' + theme_mod;
				$( class_name).attr( { 'href' : to});
			} );
		} );
	}		

	function norfolk_bind_email ( theme_mod) {
		wp.customize( theme_mod, function( value ) {
			value.bind( function( to ) {
				var class_name = 'a.' + theme_mod;
				var mailto = 'mailto:'+to;
				$( class_name).attr( { 'href' : mailto});
				$( class_name).html(to);
			} );
		} );
	}	

	norfolk_bind_html ( 'norfolk-race-time'); 
	norfolk_bind_html ( 'norfolk-race-day'); 
	norfolk_bind_html ( 'norfolk-race-month'); 
	norfolk_bind_html ( 'norfolk-race-date'); 
	norfolk_bind_html ( 'norfolk-race-year'); 

	// tophat
	norfolk_bind_href ( 'norfolk-facebook'); 
	norfolk_bind_href ( 'norfolk-email-signup'); 

	// email
	norfolk_bind_email ( 'norfolk-race-info-email'); 
	norfolk_bind_email ( 'norfolk-race-registration-email'); 
	norfolk_bind_email ( 'norfolk-race-sponsor-email'); 

	norfolk_bind_html ( 'norfolk-copyright-message'); 

	// logo
	wp.customize( 'norfolk-img-upload', function( value ) {
		value.bind( function( to ) {
				$( '.norfolk-img-upload > img' ).attr( { 'src': to});
		} );
	} );

	// font
	wp.customize( 'norfolk-font-buttons', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li > a' ).css( { 'font-family': to});
//				$( 'input[type="submit"]' ).css( { 'font-family': to});

		} );
	} );
	// font
	wp.customize( 'norfolk-font-text', function( value ) {
		value.bind( function( to ) {
				$( 'body' ).css( { 'font-family': to});
				$( '#main' ).css( { 'font-family': to});
		} );
	} );


} )( jQuery );
