/**
 * NORFOLK
 * http://underscores.me
 *
 * Theme javascript
 */

(function ($) {
  'use strict';

	// make mobile wp_nav_menu compatible with bootstrap dropdowns
	$( ".navbar-mobile li.menu-item-has-children" ).addClass( "dropdown" );
	$( ".navbar-mobile li.menu-item-has-children > a").addClass( "dropdown-toggle" );
	$( ".navbar-mobile li.menu-item-has-children > a").attr('data-toggle', 'dropdown');

	$( ".navbar-mobile ul.sub-menu" ).addClass( "dropdown-menu" );
	$( ".navbar-mobile ul.sub-menu" ).addClass( "dropdown-menu-left" );

  // search
  // mobile
  $( ".search-glass-mobile a").click( function () {
		$(".search-mobile").toggle( 400);
    $(".search-form .search-field").focus();
	});
  // desktop
	$( "li.search-glass a").click( function () {
		$(".search-desktop").toggle( 600);
    $(".search-form .search-field").focus();
    return (false);
	});

  // on search results page
  if ( $('.search').length > 0) {
    $(".search-form .search-field").focus();
  }

})(jQuery);
