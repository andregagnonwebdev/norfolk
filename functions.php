<?php
/**
 * NORFOLK functions and definitions
 *
 * @package NORFOLK
 */

define( 'LOCAL_IP_ADDR', '10.0.2.15');
define( 'DISALLOW_FILE_EDIT', true);

//echo error_log('in functions.php');
//error_log( 'test1/', 3, '/var/www/nor/wp-content/error.log');
//error_log( 'test2');


if ( ! function_exists( 'norfolk_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function norfolk_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on NORFOLK, use a find and replace
	 * to change 'norfolk' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'norfolk', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'norfolk' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
/*	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );*/

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'norfolk_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// custom logo
	add_theme_support( 'custom-logo', array(
		 'height'      => 65,
		 'width'       => 545,
	) );

	add_image_size( 'home-card', 198, 133, array( 'center', 'top'));

}
endif; // norfolk_setup
add_action( 'after_setup_theme', 'norfolk_setup' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
	 * @global int $content_width
	 */
	function norfolk_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'norfolk_content_width', 640 );
	}
	add_action( 'after_setup_theme', 'norfolk_content_width', 0 );


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function norfolk_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'norfolk' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'norfolk_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function norfolk_scripts() {

	if ( WP_DEBUG)
		$version = '4'.date( '.YmdGi'); // for DEBUG
	else
		$version = null;

	// WP required CSS
  wp_enqueue_style( 'norfolk-style-css', get_stylesheet_uri(), array(), $version ); // style.css

	// bootstrap css framework
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css', array(), $version);
	//wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css-dist/bootstrap.min.css', array(), $version);
	//wp_enqueue_style( 'font-awesome-css', 'http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css', array(), null);
	wp_enqueue_style( 'font-awesome-css', 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), null);
	// site CSS
	$css = ( WP_DEBUG) ? '/css/main.css' : '/css-dist/main.min.css';
	wp_enqueue_style( 'main-css', get_template_directory_uri() . $css, array( 'norfolk-style-css', 'bootstrap-css'), $version);


	// load google fonts
	//if ( $f = norfolk_get_theme_mod( 'norfolk-font-text')) {
	$fonts = norfolk_get_theme_fonts();
	global $norfolk_all_fonts;
	foreach ($fonts as $f) {
		if ( array_key_exists( $f, $norfolk_all_fonts) && $norfolk_all_fonts[ $f][ 1]) {
			$l = 'https://fonts.googleapis.com/css?family=';
			global $norfolk_all_fonts;
			$l .= str_replace( ' ', '+', $f) . ':' . $norfolk_all_fonts[ $f][ 1];
			$l .= '';
			wp_enqueue_style( 'google-font-'.str_replace( ' ', '-', $f), $l, array( 'norfolk-style-css', 'bootstrap-css'), null);
		}
	}

	// javascript
  	// move jquery to footer
  if (!is_admin()) {
        wp_deregister_script('jquery');
        // Load the copy of jQuery that comes with WordPress in to footer
        wp_register_script('jquery', '/wp-includes/js/jquery/jquery.js', false, $version, true);
        wp_enqueue_script('jquery');
	}

	// for watch in Gruntfile.js
	if ( in_array( $_SERVER['SERVER_ADDR'], array( '127.0.0.1', LOCAL_IP_ADDR)) || pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION) == 'dev') {
    	wp_enqueue_script( 'livereload', '//localhost:35729/livereload.js', '', false, true );
	}


	// use minified js
	$js = ( WP_DEBUG) ? 'bootstrap.js' : 'bootstrap.min.js';
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/'.$js, array('jquery'), $version, true);

	$js = ( WP_DEBUG) ? '/js/theme.js' : '/js/dist/theme.min.js';
  wp_enqueue_script( 'norfolk-theme-js', get_template_directory_uri() . $js, array('jquery'), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'norfolk_scripts' );

/**
 * Remove emoji support
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
add_action( 'init', 'norfolk_add_editor_styles' );
function norfolk_add_editor_styles() {

    add_editor_style( get_stylesheet_uri() );
		//add_editor_style( get_template_directory_uri() . '/css-dist/bootstrap.min.css');
		//add_editor_style();

}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
//require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom nav menus
 */
require get_template_directory() . '/inc/custom-nav-walker.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';
/**
 * Custom google fonts
 */
//require get_template_directory() . '/inc/google-font.php';

/**
 * Custom theme options
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Custom theme options
 */
//require get_template_directory() . '/inc/custom-theme-options.php';

/**
 * Utility functions
 */
// set up this global for some functions
$sep = '/';
$dl_filepath = dirname(dirname(dirname(dirname(__FILE__)))) . $sep . 'wp-content' . $sep . 'uploads';
require get_template_directory() . '/inc/util.php';

/**
 * Contact Form class
 */
//require get_template_directory() . '/inc/class.contact.php';

/**
 * Custom post types
 */
require get_template_directory() . '/post-types/event.php';
require get_template_directory() . '/post-types/exhibition.php';
require get_template_directory() . '/post-types/collection.php';
require get_template_directory() . '/post-types/people.php';


/**
 * WooCommerce Support
 */
// Removes showing results in Storefront theme
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

// remove sort order in products display
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'norfolk_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'norfolk_theme_wrapper_end', 10);

function norfolk_theme_wrapper_start() {
    echo '<div id="primary" class="content-area">';
    echo '<div id="main" class="site-main col-xs-12 col-md-12" role="main">';
}
function norfolk_theme_wrapper_end() {
    echo '</div><!-- end -->';
    echo '</div><!-- end -->';
}

add_action( 'after_setup_theme', 'norfolk_woocommerce_support' );
function norfolk_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Remove WooCommerce Updater
remove_action('admin_notices', 'woothemes_updater_notice');
// remove woocommerce nag message
add_filter( 'woocommerce_helper_suppress_admin_notices', '__return_true' );

// Remove default image
function woocommerce_template_loop_product_thumbnail() {
    global $post;
    if ( has_post_thumbnail() )
          echo get_the_post_thumbnail( $post->ID, 'shop_catalog' );
}

/**
 * change button text for product category
*/
add_filter ('woocommerce_product_single_add_to_cart_text', 'norfolk_add_to_cart_button');
add_filter ('woocommerce_product_add_to_cart_text', 'norfolk_add_to_cart_button');
function norfolk_add_to_cart_button( $s) {
        global $woocommerce;
		//Get product ID
		//$product_id = (int) apply_filters('woocommerce_add_to_cart_product_id', $_POST['product_id']);
		//Check if product ID is in a certain taxonomy
		//if( has_term( 'memberships', 'product_cat', $product_id ) ){
		if( has_term( 'memberships', 'product_cat') ){
					//Get cart URL
		    		return __( 'Join', 'woocommerce' );
		 }
		 else if ( has_term( 'donations', 'product_cat') ) {
			 return __( 'Support', 'woocommerce' );

		 }
		 else {
		 	return( $s);
		 };
}
add_filter( 'wc_product_sku_enabled', '__return_false' );


add_action( 'pre_get_posts', 'norfolk_pre_get_posts_query' );

function norfolk_pre_get_posts_query( $q ) {

	if ( ! $q->is_main_query() ) return;
	if ( ! $q->is_post_type_archive() ) return;

	if ( ! is_admin() && is_shop() ) {

		$q->set( 'tax_query', array(array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => array( 'memberships', 'donations' ), // Don't display products in the knives category on the shop page
			'operator' => 'NOT IN'
		)));

	}

	remove_action( 'pre_get_posts', 'norfolk_pre_get_posts_query' );

}

//add_filter( 'woocommerce_placeholder_img_src', norfolk_rpis, 10);
function norfolk_rpis( $data )
{
		return( '');
}

/**
 * Disable autofocus at checkout.
 */
function norfolk_wc_disable_autofocus_firstname( $fields ) {
	$fields['billing']['billing_first_name']['autofocus'] = false;
	if ( is_array( $fields['shipping'] ))
  	$fields['shipping']['shipping_first_name']['autofocus'] = false;

  return $fields;
}

add_filter( 'woocommerce_checkout_fields', 'norfolk_wc_disable_autofocus_firstname' );


//Display 24 products on archive pages
//add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );
